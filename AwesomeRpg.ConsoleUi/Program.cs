﻿using AwesomeRpg.ConsoleUi.Implementation;
using AwesomeRpg.Infrastructure;
using AwesomeRpg.Services.Implementation;

namespace AwesomeRpg.ConsoleUi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var startup = new Startup();
            var config = startup.GetConfiguration();
            var session = new Session();
            var gameProcessor = new GameProcessor(new ConsoleInterface(), new ScenarioPlayer(session), new ScenarioService(), session);

            gameProcessor.Process(config.ScenariesRootFolder);           
        }
    }
}
