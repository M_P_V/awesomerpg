﻿using AwesomeRpg.Domain.Models;

namespace AwesomeRpg.ConsoleUi.Interfaces
{
    public interface IUserInterface
    {
        void ShowStep(Step step);

        string GetUserInput();

        void DisplayMessage(string message);

        void Clear();
    }
}
