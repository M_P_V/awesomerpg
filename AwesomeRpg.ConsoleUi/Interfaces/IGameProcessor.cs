﻿namespace AwesomeRpg.ConsoleUi.Interfaces
{
    public interface IGameProcessor
    {
        void Process(string scenariosDirectory);
    }
}
