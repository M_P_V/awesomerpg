﻿using AwesomeRpg.Domain.Repositories.Implementation;
using AwesomeRpg.Infrastructure;

namespace AwesomeRpg.ConsoleUi
{
    public class Startup
    {
        public ApplicationConfig GetConfiguration()
        {
            var jsonDataRepository = new JsonDataRepository();

            return jsonDataRepository.GetObjectFromFile<ApplicationConfig>("appsettings.json");
        }
    }
}
