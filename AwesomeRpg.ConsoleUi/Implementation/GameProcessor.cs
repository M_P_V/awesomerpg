﻿using System.Collections.Generic;
using System.IO;
using AwesomeRpg.ConsoleUi.Interfaces;
using AwesomeRpg.Domain.Models;
using AwesomeRpg.Infrastructure;
using AwesomeRpg.Services.Interfaces;

namespace AwesomeRpg.ConsoleUi.Implementation
{
    class GameProcessor : IGameProcessor
    {
        private readonly IUserInterface _userInterface;
        private readonly IScenarioPlayer _scenarioPlayer;
        private readonly IScenarioService _scenarioService;

        private Session _session;

        public GameProcessor(IUserInterface userInterface, IScenarioPlayer scenarioPlayer, IScenarioService scenarioService, Session session)
        {
            _userInterface = userInterface;
            _scenarioPlayer = scenarioPlayer;
            _scenarioService = scenarioService;
            _session = session;
        }

        public void Process(string scenariosDirectory)
        {
            var scenaioFiles = Directory.GetFiles(scenariosDirectory);

            _session.CurrentStep = new Step
            {
                StepName = "ScenarioSelection",
                StepNotice = "Please choose one the listed scenarious.",
                Options = new List<Option>()
            };
            for (int i = 0; i < scenaioFiles.Length; i++)
            {
                var scenarioName = Path.GetFileName(scenaioFiles[i]).Replace(Path.GetExtension(scenaioFiles[i]), "");

                _session.CurrentStep.Options.Add(new Option
                {
                    OptionDescription = scenarioName,
                    OptionId = i + 1
                });
            }

            while (_session.Scenario == null)
            {
                var userInput = Execute();
                if (userInput <= scenaioFiles.Length && userInput > 0)
                {
                    _session.Scenario = _scenarioService.GetScenarioFromFile(scenaioFiles[userInput - 1]);
                }
                else
                {
                    _userInterface.DisplayMessage("Please enter valid value");
                }
            }

            _session.CurrentStep = _scenarioPlayer.GetStartStep();
            _userInterface.Clear();
            
            Execute();
        }

        private int Execute()
        {
            _userInterface.Clear();
            int userInput;

            if (_session.CurrentStep.Options == null || _session.CurrentStep.Options.Count == 0)
            {
                _userInterface.ShowStep(_session.CurrentStep);
                _userInterface.DisplayMessage("Thanks for attention.\nGoodbuye!");
                
                return default;
            }

            _userInterface.ShowStep(_session.CurrentStep);
            var userChoice = _userInterface.GetUserInput();


            if (int.TryParse(userChoice, out userInput))
            {
                if (userInput <= _session.CurrentStep.Options.Count && userInput > 0)
                {
                    if(_session.Scenario == null)
                    {
                       return userInput;
                    }

                    _scenarioPlayer.ExecuteNext(userInput);
                }
            }
            
            _userInterface.DisplayMessage("Please enter valid value");
            return Execute();

        }
    }
}
