﻿using System;
using AwesomeRpg.ConsoleUi.Interfaces;
using AwesomeRpg.Domain.Models;

namespace AwesomeRpg.ConsoleUi.Implementation
{
    public class ConsoleInterface : IUserInterface
    {
        public void ShowStep(Step step)
        {
            Console.SetCursorPosition((Console.WindowWidth - step.StepName.Length) / 2, Console.CursorTop);
            Console.WriteLine(step.StepName + "\n");   
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.WriteLine(step.StepNotice + "\n");
           
            if(step.Options != null && step.Options.Count != 0)
            {
                Console.WriteLine("You have the following options:");

                foreach (var option in step.Options)
                {
                    Console.WriteLine($"\t{ option.OptionId } - { option.OptionDescription }");
                }
                Console.WriteLine();
            }

        }

        public string GetUserInput()
        {
            Console.Write("\nYour choice:\t");
            var value = Console.ReadLine();
            Console.WriteLine();

            return value;
        }

        public void DisplayMessage(string message)
        {
            Console.WriteLine("\n" + message + "\n");
        }

        public void Clear()
        {
            Console.Clear();
        }
    }
}
