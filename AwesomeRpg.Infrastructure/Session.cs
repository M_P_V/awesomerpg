﻿using AwesomeRpg.Domain.Models;

namespace AwesomeRpg.Infrastructure
{
    public class Session
    {
        public Scenario Scenario { get; set; }
        public Step CurrentStep { get; set; }
    }
}
