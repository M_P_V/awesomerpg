﻿using AwesomeRpg.Domain.Models;

namespace AwesomeRpg.Services.Interfaces
{
    public interface IScenarioService
    {
        Scenario GetScenarioFromFile(string filePath);
    }
}
