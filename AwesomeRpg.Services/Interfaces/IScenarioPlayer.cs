﻿using AwesomeRpg.Domain.Models;

namespace AwesomeRpg.Services.Interfaces
{
    public interface IScenarioPlayer
    {
        Step GetStartStep();
        Step ExecuteNext(int currentOption);
    }
}
