﻿using AwesomeRpg.Domain.Models;
using AwesomeRpg.Infrastructure;
using AwesomeRpg.Services.Interfaces;

namespace AwesomeRpg.Services.Implementation
{
    public class ScenarioPlayer : IScenarioPlayer
    {
        private Session _session;

        public ScenarioPlayer(Session session)
        {
            _session = session;
        }

        public Step GetStartStep()
        {
            foreach (var step in _session.Scenario.Steps)
            {
                if (step.IsStart)
                {
                    _session.CurrentStep = step;

                    return step;
                }
            }

            return null;
        }

        public Step ExecuteNext(int currentOption)
        {
            foreach (var option in _session.CurrentStep.Options)
            {
                if (option.OptionId == currentOption)
                {
                    foreach (var step in _session.Scenario.Steps)
                    {
                        if (step.StepId == option.StepId)
                        {
                            _session.CurrentStep = step;

                            return step;
                        }
                    }
                }
            }

            return null;
        }
    }
}
