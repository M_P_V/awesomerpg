﻿using System.Collections.Generic;
using System.IO;
using AwesomeRpg.Domain.Models;
using AwesomeRpg.Domain.Repositories.Implementation;
using AwesomeRpg.Domain.Repositories.Interfaces;
using AwesomeRpg.Services.Interfaces;

namespace AwesomeRpg.Services.Implementation
{
    public class ScenarioService : IScenarioService
    {
        private readonly Dictionary<string, IDataRepositoryBase> _repositories;

        public ScenarioService()
        {
            _repositories = new Dictionary<string, IDataRepositoryBase>();
            _repositories.Add(".xml", new XmlDataRepository());
            _repositories.Add(".json", new JsonDataRepository());
        }

        public Scenario GetScenarioFromFile(string filePath)
        {
            var fileExtention = Path.GetExtension(filePath);

            if (_repositories.ContainsKey(fileExtention))
            {
                var targeetRepository = _repositories[fileExtention];

                return targeetRepository.GetObjectFromFile<Scenario>(filePath);
            }

            return null;
        }
    }
}
