﻿using System.Collections.Generic;

namespace AwesomeRpg.Domain.Models
{
    public class Step
    {
        public int StepId { get; set; }
        public string StepName { get; set; }
        public string StepNotice { get; set; }
        public List<Option> Options { get; set; }
        public bool IsStart { get; set; }
    }
}
