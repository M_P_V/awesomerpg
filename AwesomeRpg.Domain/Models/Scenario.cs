﻿using System.Collections.Generic;

namespace AwesomeRpg.Domain.Models
{
    public class Scenario
    {
        public List<Step> Steps { get; set; }
    }
}
