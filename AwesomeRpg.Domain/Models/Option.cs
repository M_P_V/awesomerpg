﻿namespace AwesomeRpg.Domain.Models
{
    public class Option
    {
        public int StepId { get; set; }
        public int OptionId { get; set; }
        public string OptionDescription { get; set; }

    }
}
