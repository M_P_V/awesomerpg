﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AwesomeRpg.Domain.Repositories.Interfaces
{
    public interface IDataRepositoryBase
    {
        T GetObjectFromFile<T>(string filePath);
    }
}
