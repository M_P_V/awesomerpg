﻿using System.IO;
using System.Runtime.Serialization.Json;
using AwesomeRpg.Domain.Repositories.Interfaces;

namespace AwesomeRpg.Domain.Repositories.Implementation
{
    public class JsonDataRepository : IDataRepositoryBase
    {
        public T GetObjectFromFile<T>(string filePath)
        {
            if (File.Exists(filePath))
            {
                using (var fileStream = new FileStream(filePath, FileMode.Open))
                {
                    var jsonSerializer = new DataContractJsonSerializer(typeof(T));

                    return (T)jsonSerializer.ReadObject(fileStream);
                }
            }

            return default;
        }
    }
}
