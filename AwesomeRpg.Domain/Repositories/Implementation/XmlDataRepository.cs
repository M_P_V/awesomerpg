﻿using System.IO;
using System.Xml.Serialization;
using AwesomeRpg.Domain.Repositories.Interfaces;

namespace AwesomeRpg.Domain.Repositories.Implementation
{
    public class XmlDataRepository : IDataRepositoryBase
    {
        public T GetObjectFromFile<T>(string filePath)
        {
            if (File.Exists(filePath))
            {
                using (var fileStream = new FileStream(filePath, FileMode.Open))
                {
                    var xmlSerializer = new XmlSerializer(typeof(T));

                    return (T)xmlSerializer.Deserialize(fileStream);
                }
            }

            return default;
        }
    }
}
